# VisionKit
Eredo!!! Computer Vision Algorithms Library. \
Make a companion website for these and publish many tools that can be used e.g Point Cloud Viewer online, Image segmentation model online etc. This could be an
iCloud style web app, where there is an app for working with 2D images, 3D images, ML Models etc. We are basically creating an ecosystem where the apps works
perfectly together - An there is the convenience of easy access to your data
* https://developer.apple.com/documentation/vision

# Dependencies 
* OpenCV
* Testing framework https://github.com/Snaipe/Criterion or CTest https://cmake.org/Wiki/CMake/Testing_With_CTest

# Notes
* Convert OpenCV Matrix to Eigen 
	* https://stackoverflow.com/questions/14783329/opencv-cvmat-and-eigenmatrix
	* https://stackoverflow.com/questions/16451111/cvmat-conversion-to-eigen-matrix-and-back/16544090

# TODO (Tooling)
- [ ] Include test support
- [ ] Include cmake cross-platform support
- [ ] Add SWIG support (NodeJS Extensions)

# TODO (Feature Extraction)
- [ ] Sobel Edge Detection (image,kernel_size) -> (edge_magnitude,edge_directions)
